CREATE TABLE Ingredients (
    id INTEGER PRIMARY KEY,
    short_name TEXT,
    quantity INT,
    date_in DATE,
    date_use_by DATE
);

INSERT INTO Ingredients (short_name, quantity, date_in, date_use_by)
    VALUES('Canned corn, 14.5oz',6,NULL,NULL);
INSERT INTO Ingredients (short_name, quantity, date_in, date_use_by) 
    VALUES('Canned peas, 14.5oz',3,NULL,NULL);
INSERT INTO Ingredients (short_name, quantity, date_in, date_use_by) 
    VALUES('Canned green beans, 14.5oz',3,NULL,NULL);
INSERT INTO Ingredients (short_name, quantity, date_in, date_use_by) 
    VALUES('Canned carrots, 14.5oz',3,NULL,NULL);
INSERT INTO Ingredients (short_name, quantity, date_in, date_use_by) 
    VALUES('Canned peas, 8oz',3,NULL,NULL);
INSERT INTO Ingredients (short_name, quantity, date_in, date_use_by) 
    VALUES('Canned corn, 8oz',4,NULL,NULL);
INSERT INTO Ingredients (short_name, quantity, date_in, date_use_by) 
    VALUES('Whipping cream, 1pt',1,'2019-11-02','2019-12-23');
INSERT INTO Ingredients (short_name, quantity, date_in, date_use_by) 
    VALUES('Chicken breast, 1lb', 2,'2019-11-02', NULL);
INSERT INTO Ingredients (short_name, quantity, date_in, date_use_by) 
    VALUES('Pork chops, thin, 9', 1,'2019-11-02',NULL);
INSERT INTO Ingredients (short_name, quantity, date_in, date_use_by) 
    VALUES('Turkey sausage', 1,'2019-08-04',NULL);
INSERT INTO Ingredients (short_name, quantity, date_in, date_use_by) 
    VALUES('Ground turkey',1,'2019-11-02',NULL);
INSERT INTO Ingredients (short_name, quantity, date_in, date_use_by) 
    VALUES('Red pasta sauce',1,'2019-11-02',NULL);
INSERT INTO Ingredients (short_name, quantity, date_in, date_use_by) 
    VALUES('Hot dogs',6,NULL,NULL);
INSERT INTO Ingredients (short_name, quantity, date_in, date_use_by) 
    VALUES('Salmon',2,'2019-11-02',NULL);
