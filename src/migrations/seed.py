
def seed_database(connection):
    
    with open('../data/DataModel.sql', 'r') as dmscript:
        sql = dmscript.read()
    
    connection.executescript(sql)