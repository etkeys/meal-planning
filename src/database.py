from pathlib import Path
import os
import sqlite3

_database_path = "../data/app.db"

def rebuild():
    from migrations import seed

    _create_database(overwrite=True)

    with sqlite3.connect(_database_path) as conn:
        seed.seed_database(conn)
        

def _create_database(**kwargs):
    overwrite = False
    if 'overwrite' in kwargs:
        overwrite = kwargs['overwrite']

    if _database_exists():
        if not overwrite:
            return
        
        os.remove(_database_path)
    
    with open(_database_path,'a'):
        pass


def _database_exists():
    return Path(_database_path).is_file()


