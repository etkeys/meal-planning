from views import view

class MainView(view.View):

    _header = '\tMain menu\n'

    def __init__(self):
        self._options = [
            ('i','(i)ngredients',self._ingredients_menu),
            ('m','(m)eals',self._meals_menu),
            ('s','(s)chedule',self._schedule_menu),
            ('e','(e)xit',self._exit_menu)
        ]
    
    def _ingredients_menu(self):
        print('\nNavigated to ingredients menu\n')

    def _meals_menu(self):
        print('\nNavigated to meals menu\n')

    def _schedule_menu(self):
        print('\nNavigated to schedule menu\n')