
_OPT_KEY_ = 0
_OPT_MSG_ = 1
_OPT_FUNC_ = 2

class View:
    
    _footer = ''
    _header = ''
    _options = [('','',"<func()>"),('','',"<func()>")]
    _is_closed = False

    def __init__(self):
        pass
    
    def __str__(self):
        s = []
        s.append(f'\n{self._header}')

        for _, optmsg, _ in self._options:
            s.append(optmsg)
        
        s.append(self._footer)

        return '\n'.join(s)

    def _exit_menu(self):
        self._is_closed = True

    def is_closed(self):
        return self._is_closed
    
    def show(self):
        
        done = False
        while not done:
            print(self)
            
            # TODO Default choice?
            select = input('Please enter selection: ').lower()[0]
            opt = self._get_option(select)

            if opt is None:
                print(f'Unknown selection: {select}')
            else:
                opt[_OPT_FUNC_]()
            
            done = self.is_closed()
    
    def _get_option(self, selection):
        for key, optmsg, action in self._options:
            if selection == key:
                return (key, optmsg, action)

        return None





