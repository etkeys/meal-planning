import sys

import database
from views import main_view

def main(argv):
    if '--rebuild-db' in argv:
        database.rebuild()
    
    view = main_view.MainView()
    view.show()

    print('\nExiting.\n')


##### Entry point #####
if __name__ == '__main__':
    main(sys.argv)