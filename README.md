# Meal planning

Based on the ingredients you have, generate a schedule of meals. Idea is to
have a rotating menu. Leftovers are considered and are carried over to future
meals.